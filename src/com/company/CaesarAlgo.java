package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CaesarAlgo extends MovingAlgo {

    String[] alphabet = "abcdefghijklmnopqrstuvwxyz".split("", 50);

    @Override
    public char encode_char(char c, int key) {
        System.out.println(Arrays.toString(alphabet));
        String char_c = Character.toString(c);
        int index;

        for (int i=0;i<alphabet.length;i++) {

            if (char_c.equals(alphabet[i])) {
                System.out.println(i + key);
                if (i + key > 25) {
                    index = (i + key) % 26;
                }
                else if (i + key < 0) {
                    index = 26 + (i + key);
                }
                else {index = i + key;}

                return alphabet[index].charAt(0);
            }
        }
        return c;
    }
}
