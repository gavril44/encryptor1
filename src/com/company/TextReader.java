package com.company;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class TextReader {

    public List<String> read_file_in_list(String file_location) {
        /* takes the file location given and returns
         *  a list of strings of each line */

        List<String> lines = Collections.emptyList();
        try {
            lines = Files.readAllLines(Paths.get(file_location), StandardCharsets.UTF_8);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
