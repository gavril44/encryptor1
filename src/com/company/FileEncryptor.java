package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class FileEncryptor {

    String NEW_FILE_DIRECTORY = "C:\\ofek_work\\dir_test1";
    EncryptionAlgo encryptionAlgo;

    FileMaker fileMaker = new FileMaker(NEW_FILE_DIRECTORY);
    TextReader textReader = new TextReader();

    public FileEncryptor(EncryptionAlgo encryptionAlgo) {
        this.encryptionAlgo = encryptionAlgo;

    }

    public void encrypt(String file_location, int key) {

        String new_file_location = fileMaker.make_new_file_location_name(file_location, "encoded");
        String key_file_location = NEW_FILE_DIRECTORY + "\\key.txt";

        fileMaker.open_a_new_file(new_file_location);
        fileMaker.open_a_new_file(key_file_location);

        try {
            FileWriter writer = new FileWriter(new_file_location, true);
            // loop throw lines

            List<String> line_list = textReader.read_file_in_list(file_location);
            List<String> new_line_list = encryptionAlgo.encode(line_list, key);

            for (String str_line : new_line_list) {
                System.out.println(str_line);
                writer.write(str_line);
                writer.write("\r\n");
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileWriter writer2 = new FileWriter(key_file_location, true);
            writer2.write("your key is:\n" + key);
            writer2.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void decrypt(String file_location, String key_path) {
        String new_file_location = fileMaker.make_new_file_location_name(file_location, "decrypted");
        fileMaker.open_a_new_file(new_file_location);

        List<String> key = textReader.read_file_in_list(key_path);
        int key_f = Integer.parseInt(key.get(1));
        System.out.println(key_f);

        try {
            FileWriter writer = new FileWriter(new_file_location, true);
            // loop throw lines

            List<String> line_list = textReader.read_file_in_list(file_location);
            List<String> new_line_list = encryptionAlgo.encode(line_list, -1 * key_f);

            for (String str_line : new_line_list) {
                System.out.println(str_line);
                writer.write(str_line);
                writer.write("\r\n");
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
