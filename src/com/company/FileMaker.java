package com.company;

import java.io.File;
import java.io.IOException;

public class FileMaker {

    String NEW_FILE_DIRECTORY;

    public FileMaker(String new_directory) {
        this.NEW_FILE_DIRECTORY = new_directory;
    }

    public String make_new_file_location_name(String old_file_location, String ending) {
        int indx_of_start = 0;

        for (int i=old_file_location.length()-1;i>-1;i--) {
            char ch = old_file_location.charAt(i);
            if (ch == '\\') {
                indx_of_start = i;
                break;
            }

        }
        String old_name = old_file_location.substring(indx_of_start);
        String[] old_name_list = old_name.split("\\.", 10);

        return NEW_FILE_DIRECTORY + "\\" + old_name_list[0] + "_" + ending + ".txt";
    }

    public void open_a_new_file(String new_location) {

        File myfile = new File(new_location);
        try {
            if (myfile.createNewFile()) {
                System.out.println("your file was created in: " + new_location + "\n\n");
            }
            else {
                System.out.println("File already exist");
            }
        }
        catch (IOException e) {
            System.out.println("error");
            e.printStackTrace();
        }
    }

    public void set_new_file_directori(String new_file_directori) {
        this.NEW_FILE_DIRECTORY = new_file_directori;
    }
}
