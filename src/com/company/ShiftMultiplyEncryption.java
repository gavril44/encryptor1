package com.company;

public class ShiftMultiplyEncryption extends CaesarAlgo {

    @Override
    public char encode_char(char c, int key) {

        int letter_index = c - 97;
        int new_key = (letter_index * key) - letter_index;
        return super.encode_char(c, new_key);
    }
}
