package com.company;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovingAlgo implements EncryptionAlgo {

    public List<String> encode(List<String> line_list, int key) {

        List<String> new_list = new ArrayList();
        int line_num = 0;

        for (String str_line : line_list) {
            new_list.add("");

            for (int i = 0; i < str_line.length(); i++) {
                char c = str_line.charAt(i);
                char new_char = encode_char(c, key);
                new_list.set(line_num, new_list.get(line_num) + String.valueOf(new_char));
            }
            line_num++;
        }
        return new_list;
    }


    public char encode_char(char c, int key) {

        int char_num = c;

        char_num = (char_num + key) % 127;
        if (char_num < 32 && key >= 0) {
            char_num = char_num + 32;
        }
        else if (char_num < 32 && key < 0) {
            char_num = 127 - (32-char_num);
        }
        return (char)char_num;
    }

}


