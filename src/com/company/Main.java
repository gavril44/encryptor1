package com.company;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
        String NEW_FILE_DIRECTORY = "C:\\ofek_work\\dir_test1";
        int MAX_OFSET_KEY = 31;
        int MIN_OFSET_KEY = 0;
        int random_key = ThreadLocalRandom.current().nextInt(MIN_OFSET_KEY, MAX_OFSET_KEY);

        Scanner input = new Scanner(System.in);

        System.out.println("Hello! Welcome to gavries encr/decr 2000\n");
        System.out.println("For encrypting enter: encrypt file_location.");
        System.out.println("For decrypting enter: decrypt file_location");

        String user_request = input.nextLine();

        Checker checker = new Checker(user_request);
        EncryptionAlgo encryptionAlgo = new CaesarAlgo();
        FileEncryptor encryptor = new FileEncryptor(encryptionAlgo);

        if (checker.check_intput()) {
            String request_file_name = checker.get_file_location();

            System.out.println("checked");
            if (checker.get_command().equals("enc")) {
                // code encrypting
                encryptor.encrypt(request_file_name, random_key);
            }
            else {
                // code decrypting
                System.out.println("Please enter a key path: ");
                String key = input.nextLine();

                encryptor.decrypt(request_file_name, key);
            }
        }
        else {
            System.out.println("invalid request, try again\n\n");
            main(args);
        }
    }
}

