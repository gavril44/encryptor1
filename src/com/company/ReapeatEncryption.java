package com.company;

import java.util.ArrayList;
import java.util.List;

public class ReapeatEncryption extends FileEncryptor {

    int number_of_cycle;

    public ReapeatEncryption(int number_of_cycle, EncryptionAlgo encryptionAlgo) {
        super(encryptionAlgo);
        this.number_of_cycle = number_of_cycle;

    }

    @Override
    public List<String> encode_list(List<String> line_list, EncryptionAlgo encryptionAlgo, int key) {

        List<String> line_list1 = new ArrayList<String>();
        List<String> line_list2 = line_list;

        for (int i=0;i<number_of_cycle;i++) {
            line_list1 = super.encode_list(line_list2, encryptionAlgo, key);
            line_list2 = line_list1;
        }
        return line_list1;
    }
}
