package com.company;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Checker {

    String[] request_list;

    public Checker(String input_request) {
        this.request_list = input_request.split(" ", 4);

    }

    public boolean check_intput() {

        if (request_list[0].equals("enc") || request_list[0].equals("dec")) {

            File file = new File(request_list[1]);
            if (file.isFile()) {
                return true;
            }
        }
        return false;
    }

    public String get_command() {
        return request_list[0];
    }

    public String get_file_location() {
        return request_list[1];
    }

    public String get_file_name() {
        String[] file_location_list = request_list[1].split("/", 10);
        String file_name = file_location_list[-1];
        return file_name;
    }

}

